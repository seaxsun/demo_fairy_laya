(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class GameControl extends Laya.Script {
        constructor() {
            super();
            this.createBoxInterval = 1000;
            this._time = 0;
            this._started = false;
        }
        onEnable() {
            this._time = Date.now();
            this._gameBox = this.owner.getChildByName("gameBox");
        }
        onUpdate() {
            let now = Date.now();
            if (now - this._time > this.createBoxInterval && this._started) {
                this._time = now;
                this.createBox();
            }
        }
        createBox() {
            let box = Laya.Pool.getItemByCreateFun("dropBox", this.dropBox.create, this.dropBox);
            box.pos(Math.random() * (Laya.stage.width - 100), -100);
            this._gameBox.addChild(box);
        }
        onStageClick(e) {
            e.stopPropagation();
            let flyer = Laya.Pool.getItemByCreateFun("bullet", this.bullet.create, this.bullet);
            flyer.pos(Laya.stage.mouseX, Laya.stage.mouseY);
            this._gameBox.addChild(flyer);
        }
        startGame() {
            if (!this._started) {
                this._started = true;
                this.enabled = true;
            }
        }
        stopGame() {
            this._started = false;
            this.enabled = false;
            this.createBoxInterval = 1000;
            this._gameBox.removeChildren();
        }
    }

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            GameUI.instance = this;
            Laya.MouseManager.multiTouchEnabled = false;
        }
        onEnable() {
            this._control = this.getComponent(GameControl);
            this.tipLbll.on(Laya.Event.CLICK, this, this.onTipClick);
        }
        onTipClick(e) {
            this.tipLbll.visible = false;
            this._score = 0;
            this.scoreLbl.text = "";
            this._control.startGame();
        }
        addScore(value = 1) {
            this._score += value;
            this.scoreLbl.changeText("分数：" + this._score);
            if (this._control.createBoxInterval > 600 && this._score % 20 == 0)
                this._control.createBoxInterval -= 20;
        }
        stopGame() {
            this.tipLbll.visible = true;
            this.tipLbll.text = "游戏结束了，点击屏幕重新开始";
            this._control.stopGame();
        }
    }

    class Bullet extends Laya.Script {
        constructor() { super(); }
        onEnable() {
            var rig = this.owner.getComponent(Laya.RigidBody);
            rig.setVelocity({ x: 0, y: -10 });
        }
        onTriggerEnter(other, self, contact) {
            this.owner.removeSelf();
        }
        onUpdate() {
            if (this.owner.y < -10) {
                this.owner.removeSelf();
            }
        }
        onDisable() {
            Laya.Pool.recover("bullet", this.owner);
        }
    }

    class DropBox extends Laya.Script {
        constructor() {
            super();
            this.level = 1;
        }
        onEnable() {
            this._rig = this.owner.getComponent(Laya.RigidBody);
            this.level = Math.round(Math.random() * 5) + 1;
            this._text = this.owner.getChildByName("levelTxt");
            this._text.text = this.level + "";
        }
        onUpdate() {
            this.owner.rotation++;
        }
        onTriggerEnter(other, self, contact) {
            var owner = this.owner;
            if (other.label === "buttle") {
                if (this.level > 1) {
                    this.level--;
                    this._text.changeText(this.level + "");
                    owner.getComponent(Laya.RigidBody).setVelocity({ x: 0, y: -10 });
                    Laya.SoundManager.playSound("sound/hit.wav");
                }
                else {
                    if (owner.parent) {
                        let effect = Laya.Pool.getItemByCreateFun("effect", this.createEffect, this);
                        effect.pos(owner.x, owner.y);
                        owner.parent.addChild(effect);
                        effect.play(0, true);
                        owner.removeSelf();
                        Laya.SoundManager.playSound("sound/destroy.wav");
                    }
                }
                GameUI.instance.addScore(1);
            }
            else if (other.label === "ground") {
                owner.removeSelf();
                GameUI.instance.stopGame();
            }
        }
        createEffect() {
            let ani = new Laya.Animation();
            ani.loadAnimation("test/TestAni.ani");
            ani.on(Laya.Event.COMPLETE, null, recover);
            function recover() {
                ani.removeSelf();
                Laya.Pool.recover("effect", ani);
            }
            return ani;
        }
        onDisable() {
            Laya.Pool.recover("dropBox", this.owner);
        }
    }

    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
            reg("script/GameControl.ts", GameControl);
            reg("script/Bullet.ts", Bullet);
            reg("script/DropBox.ts", DropBox);
        }
    }
    GameConfig.width = 1136;
    GameConfig.height = 570;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = false;
    GameConfig.init();

    class BasicDemo {
        constructor() {
            this.startPos = new Laya.Point();
            fgui.UIConfig.verticalScrollBar = "ui://Basics/ScrollBar_VT";
            fgui.UIConfig.horizontalScrollBar = "ui://Basics/ScrollBar_HZ";
            fgui.UIConfig.popupMenu = "ui://Basics/PopupMenu";
            fgui.UIConfig.buttonSound = "ui://Basics/click";
            fgui.UIPackage.loadPackage("res/UI/Basics", Laya.Handler.create(this, this.onUILoaded));
        }
        onUILoaded() {
            this._view = fgui.UIPackage.createObject("Basics", "Main").asCom;
            this._view.makeFullScreen();
            fgui.GRoot.inst.addChild(this._view);
            this._backBtn = this._view.getChild("btn_Back");
            this._backBtn.visible = false;
            this._backBtn.onClick(this, this.onClickBack);
            this._demoContainer = this._view.getChild("container").asCom;
            this._cc = this._view.getController("c1");
            var cnt = this._view.numChildren;
            for (var i = 0; i < cnt; i++) {
                var obj = this._view.getChildAt(i);
                if (obj.group != null && obj.group.name == "btns")
                    obj.onClick(this, this.runDemo);
            }
            this._demoObjects = {};
        }
        destroy() {
            fgui.UIConfig.verticalScrollBar = "";
            fgui.UIConfig.horizontalScrollBar = "";
            fgui.UIConfig.popupMenu = "";
            fgui.UIConfig.buttonSound = "";
            fgui.UIPackage.removePackage("Basics");
        }
        runDemo(evt) {
            var type = fgui.GObject.cast(evt.currentTarget).name.substr(4);
            var obj = this._demoObjects[type];
            if (obj == null) {
                obj = fgui.UIPackage.createObject("Basics", "Demo_" + type).asCom;
                this._demoObjects[type] = obj;
            }
            this._demoContainer.removeChildren();
            this._demoContainer.addChild(obj);
            this._cc.selectedIndex = 1;
            this._backBtn.visible = true;
            switch (type) {
                case "Button":
                    this.playButton();
                    break;
                case "Text":
                    this.playText();
                    break;
                case "Window":
                    this.playWindow();
                    break;
                case "Popup":
                    this.playPopup();
                    break;
                case "Drag&Drop":
                    this.playDragDrop();
                    break;
                case "Depth":
                    this.playDepth();
                    break;
                case "Grid":
                    this.playGrid();
                    break;
                case "ProgressBar":
                    this.playProgressBar();
                    break;
            }
        }
        onClickBack(evt) {
            this._cc.selectedIndex = 0;
            this._backBtn.visible = false;
        }
        playButton() {
            var obj = this._demoObjects["Button"];
            obj.getChild("n34").onClick(this, this.__clickButton);
        }
        __clickButton() {
            console.log("click button");
        }
        playText() {
            var obj = this._demoObjects["Text"];
            obj.getChild("n12").on(Laya.Event.LINK, this, this.__clickLink);
            obj.getChild("n25").onClick(this, this.__clickGetInput);
        }
        __clickLink(link) {
            var obj = this._demoObjects["Text"];
            obj.getChild("n12").text = "[img]ui://9leh0eyft9fj5f[/img][color=#FF0000]你点击了链接[/color]：" + link;
        }
        __clickGetInput() {
            var obj = this._demoObjects["Text"];
            obj.getChild("n24").text = obj.getChild("n22").text;
        }
        playWindow() {
            var obj = this._demoObjects["Window"];
        }
        playPopup() {
            if (this._pm == null) {
                this._pm = new fgui.PopupMenu();
                this._pm.addItem("Item 1");
                this._pm.addItem("Item 2");
                this._pm.addItem("Item 3");
                this._pm.addItem("Item 4");
                if (this._popupCom == null) {
                    this._popupCom = fgui.UIPackage.createObject("Basics", "Component12").asCom;
                    this._popupCom.center();
                }
            }
            var obj = this._demoObjects["Popup"];
            var btn = obj.getChild("n0");
            btn.onClick(this, this.__clickPopup1);
            var btn2 = obj.getChild("n1");
            btn2.onClick(this, this.__clickPopup2);
        }
        __clickPopup1(evt) {
            var btn = fgui.GObject.cast(evt.currentTarget);
            this._pm.show(btn, true);
        }
        __clickPopup2() {
            fgui.GRoot.inst.showPopup(this._popupCom);
        }
        playDragDrop() {
            var obj = this._demoObjects["Drag&Drop"];
            var btnA = obj.getChild("a");
            btnA.draggable = true;
            var btnB = obj.getChild("b").asButton;
            btnB.draggable = true;
            btnB.on(fgui.Events.DRAG_START, this, this.__onDragStart);
            var btnC = obj.getChild("c").asButton;
            btnC.icon = null;
            btnC.on(fgui.Events.DROP, this, this.__onDrop);
            var btnD = obj.getChild("d");
            btnD.draggable = true;
            var bounds = obj.getChild("bounds");
            var rect = bounds.localToGlobalRect(0, 0, bounds.width, bounds.height);
            rect = fgui.GRoot.inst.globalToLocalRect(rect.x, rect.y, rect.width, rect.height, rect);
            rect.x -= obj.parent.x;
            btnD.dragBounds = rect;
        }
        __onDragStart(evt) {
            var btn = fgui.GObject.cast(evt.currentTarget);
            btn.stopDrag();
            fgui.DragDropManager.inst.startDrag(btn, btn.icon, btn.icon);
        }
        __onDrop(data, evt) {
            var btn = fgui.GObject.cast(evt.currentTarget);
            btn.icon = data;
        }
        playDepth() {
            var obj = this._demoObjects["Depth"];
            var testContainer = obj.getChild("n22").asCom;
            var fixedObj = testContainer.getChild("n0");
            fixedObj.sortingOrder = 100;
            fixedObj.draggable = true;
            var numChildren = testContainer.numChildren;
            var i = 0;
            while (i < numChildren) {
                var child = testContainer.getChildAt(i);
                if (child != fixedObj) {
                    testContainer.removeChildAt(i);
                    numChildren--;
                }
                else
                    i++;
            }
            this.startPos.x = fixedObj.x;
            this.startPos.y = fixedObj.y;
            obj.getChild("btn0").onClick(this, this.__click1);
            obj.getChild("btn1").onClick(this, this.__click2);
        }
        __click1() {
            var graph = new fgui.GGraph();
            this.startPos.x += 10;
            this.startPos.y += 10;
            graph.setXY(this.startPos.x, this.startPos.y);
            graph.setSize(150, 150);
            graph.drawRect(1, "#000000", "#FF0000");
            var obj = this._demoObjects["Depth"];
            obj.getChild("n22").asCom.addChild(graph);
        }
        __click2() {
            var graph = new fgui.GGraph();
            this.startPos.x += 10;
            this.startPos.y += 10;
            graph.setXY(this.startPos.x, this.startPos.y);
            graph.setSize(150, 150);
            graph.drawRect(1, "#000000", "#00FF00");
            graph.sortingOrder = 200;
            var obj = this._demoObjects["Depth"];
            obj.getChild("n22").asCom.addChild(graph);
        }
        playGrid() {
            var obj = this._demoObjects["Grid"];
            var list1 = obj.getChild("list1").asList;
            list1.removeChildrenToPool();
            var testNames = ["苹果手机操作系统", "安卓手机操作系统", "微软手机操作系统", "微软桌面操作系统", "苹果桌面操作系统", "未知操作系统"];
            var testColors = [0xFFFF00, 0xFF0000, 0xFFFFFF, 0x0000FF];
            var cnt = testNames.length;
            for (var i = 0; i < cnt; i++) {
                var item = list1.addItemFromPool().asButton;
                item.getChild("t0").text = "" + (i + 1);
                item.getChild("t1").text = testNames[i];
                item.getChild("t2").asTextField.color = Laya.Utils.toHexColor(testColors[Math.floor(Math.random() * 4)]);
                item.getChild("star").asProgress.value = (Math.floor(Math.random() * 3) + 1) / 3 * 100;
            }
            var list2 = obj.getChild("list2").asList;
            list2.removeChildrenToPool();
            for (var i = 0; i < cnt; i++) {
                var item = list2.addItemFromPool().asButton;
                item.getChild("cb").asButton.selected = false;
                item.getChild("t1").text = testNames[i];
                item.getChild("mc").asMovieClip.playing = i % 2 == 0;
                item.getChild("t3").text = "" + Math.floor(Math.random() * 10000);
            }
        }
        playProgressBar() {
            var obj = this._demoObjects["ProgressBar"];
            Laya.timer.frameLoop(2, this, this.__playProgress);
            obj.on(Laya.Event.UNDISPLAY, this, this.__removeTimer);
        }
        __removeTimer() {
            Laya.timer.clear(this, this.__playProgress);
        }
        __playProgress() {
            var obj = this._demoObjects["ProgressBar"];
            var cnt = obj.numChildren;
            for (var i = 0; i < cnt; i++) {
                var child = obj.getChildAt(i);
                if (child != null) {
                    child.value += 1;
                    if (child.value > child.max)
                        child.value = 0;
                }
            }
        }
    }

    class MainMenu {
        constructor() {
            fgui.UIPackage.loadPackage("res/UI/MainMenu", Laya.Handler.create(this, this.onUILoaded));
        }
        onUILoaded() {
            this._view = fgui.UIPackage.createObject("MainMenu", "Main").asCom;
            this._view.makeFullScreen();
            fgui.GRoot.inst.addChild(this._view);
            this._view.getChild("n1").onClick(this, function () {
                this.startDemo(BasicDemo);
            });
        }
        startDemo(demoClass) {
            this._view.dispose();
            let demo = new demoClass();
            Laya.stage.event("start_demo", demo);
        }
        destroy() {
            this._view.dispose();
        }
    }

    class DemoEntry {
        constructor() {
            Laya.stage.on("start_demo", this, this.onDemoStart);
            this._currentDemo = new MainMenu();
        }
        onDemoStart(demo) {
            this._currentDemo = demo;
            this._closeButton = fgui.UIPackage.createObject("MainMenu", "CloseButton");
            this._closeButton.setXY(fgui.GRoot.inst.width - this._closeButton.width - 10, fgui.GRoot.inst.height - this._closeButton.height - 10);
            this._closeButton.addRelation(fgui.GRoot.inst, fgui.RelationType.Right_Right);
            this._closeButton.addRelation(fgui.GRoot.inst, fgui.RelationType.Bottom_Bottom);
            this._closeButton.sortingOrder = 100000;
            this._closeButton.onClick(this, this.onDemoClosed);
            fgui.GRoot.inst.addChild(this._closeButton);
        }
        onDemoClosed() {
            if (this._currentDemo.destroy)
                this._currentDemo.destroy();
            fgui.GRoot.inst.removeChildren(0, -1, true);
            this._currentDemo = new MainMenu();
        }
    }

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.stage.bgColor = "#333333";
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            Laya.stage.addChild(fgui.GRoot.inst.displayObject);
            new DemoEntry();
        }
    }
    new Main();

}());
//# sourceMappingURL=bundle.js.map
