import BasicDemo from "./BasicsDemo";


export default class MainMenu {
    // 指定 view 為 GComponent
    private _view: fgui.GComponent;

    constructor() {
        // 創建包顯示，用 Laya.Handler 方法，好處是會自動回收，創造出「容器」
        fgui.UIPackage.loadPackage("res/UI/MainMenu", Laya.Handler.create(this, this.onUILoaded));
    }

    onUILoaded() {
        // 將 Main 裝入 _view
        this._view = fgui.UIPackage.createObject("MainMenu", "Main").asCom;
        // 全瑩幕？
        this._view.makeFullScreen();
        // 增加至 GObject 成為子對像 顯示在舞台上
        fgui.GRoot.inst.addChild(this._view);
        // 取得 _view 的子物件 n1 滑鼠事件。點擊時，執行 MainMenu 裡的 startDemo()
        this._view.getChild("n1").onClick(this, function () {
            this.startDemo(BasicDemo);// 把import basicDemo 導入至 startDemo()裡
        });
    }

    startDemo(demoClass: any): void {
        // 將 MainMenu 的 Main 刪除
        this._view.dispose();
        // 實例化 demoClass = BasicDemo
        let demo: any = new demoClass();
        // 將 實例化 BasicDemo 派發 事件 ？？（ 查 Laya api）
        Laya.stage.event("start_demo", demo);
    }

    destroy() {
        // GComponent 消滅的方法
        this._view.dispose();
    }
}