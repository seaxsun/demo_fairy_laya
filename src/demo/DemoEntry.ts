import MainMenu from "./MainMenu";

export default class DemoEntry {
    // 定義私有屬性
    private _closeButton: fgui.GObject;
    private _currentDemo: any;

    constructor() {
        // 滑鼠等待 等著事件 指向「 start_demo」就可以執行 onDemoSatrt
        // MainMenu 的startDemo 裡的 demo 就會被放到這裡
        Laya.stage.on("start_demo", this, this.onDemoStart);
        // 實例化 MainMenu 
        this._currentDemo = new MainMenu();
    }

    onDemoStart(demo) {
        // 清空 _currentDemo 從 MainMenu 變 demo
        this._currentDemo = demo;
        // 創建物體 參數1：包的名稱；參數2：來源名稱
        this._closeButton = fgui.UIPackage.createObject("MainMenu", "CloseButton");
        // 位置
        this._closeButton.setXY(fgui.GRoot.inst.width - this._closeButton.width - 10, fgui.GRoot.inst.height - this._closeButton.height - 10);
        // 關聯：右右
        this._closeButton.addRelation(fgui.GRoot.inst, fgui.RelationType.Right_Right);
        // 關聯：下下
        this._closeButton.addRelation(fgui.GRoot.inst, fgui.RelationType.Bottom_Bottom);
        // 渲染順序，頂置到最前面
        this._closeButton.sortingOrder = 100000;
        // FGUI的滑鼠事件，當按下時，就會執行 DemoEntry.onDemmoClosed
        this._closeButton.onClick(this, this.onDemoClosed);
        // 將_closeButton 裝進 組件裡？ 變成一個 GObject 的 子節點
        fgui.GRoot.inst.addChild(this._closeButton);
    }

    onDemoClosed() {
        // 如果他有 destroy 方法，我們就「執行」 destroy() 
        if (this._currentDemo.destroy)
            // 執行消滅（destroy 找不到定義）
            this._currentDemo.destroy();
        // 開始為 0 到 -1 都移除掉；-1 應該是指「全部」
        fgui.GRoot.inst.removeChildren(0, -1, true);
        // 重新再創建 Main Menu
        this._currentDemo = new MainMenu();
    }
}
